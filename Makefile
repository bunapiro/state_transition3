# make file

# マクロ定義
ProgramName = FSM
ProgramObjects = Main.o SubState.o State.o FSM.o
CC = gcc

# プライマリターゲット
$(ProgramName): $(ProgramObjects)
	$(CC) -o $(ProgramName) $(ProgramObjects)

# **.oファイルは**.cファイルから生成する
.SUFFIXES: .c .o
.c.o:
	$(CC) -c $<

# オブジェクトファイルを削除
.PHONY: Clean
Clean:
	$(RM) $(ProgramName) $(ProgramObjects)

