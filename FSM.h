#ifndef FSM_H_INCLUDED
#define FSM_H_INCLUDED

struct State;

/* 有限状態機械(FSM)クラス */
typedef struct FSM {
	const struct State *current_state;    /* 現在の状態 */
	const struct State *current_substate; /* 現在のサブ状態 */
	const struct State *last_substate;    /* サブ状態の前回の状態 */
} FSM;

PUBLIC void FSM_init( FSM *pSelf );

PUBLIC void FSM_run( FSM *pSelf );    /* 運転開始イベント */
PUBLIC void FSM_stop( FSM *pSelf );   /* 運転停止イベント */
PUBLIC void FSM_switch( FSM *pSelf ); /* 運転切替イベント */

/* 状態遷移 */
PUBLIC void FSM_change_state(FSM *self, const struct State *new_state);

/* 状態遷移(サブ状態用) */
PUBLIC void FSM_change_substate(FSM *self, const struct State *new_state);

#endif /* FSM_H_INCLUDED */


