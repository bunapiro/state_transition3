#include <stdio.h>
#include "../../include/local.h"
#include "State.h"

PRIVATE void SubStateCooling_switch( FSM *pFsm );
PRIVATE void SubStateWarming_switch( FSM *pFsm );
PRIVATE void SubStateDehumidifying_switch( FSM *pFsm );

/* eStateΜBκΜIuWFNg */                /* entry */    /* exit */      /* event_run */     /* event_stop */    /* event_switch */
PRIVATE const State m_substate_Cooling       = { NULL,          NULL,           NULL,               NULL,               SubStateCooling_switch };
PRIVATE const State m_substate_Warming       = { NULL,          NULL,           NULL,               NULL,               SubStateWarming_switch };
PRIVATE const State m_substate_Dehumidifying = { NULL,          NULL,           NULL,               NULL,               SubStateDehumidifying_switch };

/* TuσΤΜϊσΤΜέθ */
PUBLIC void init_last_substate( FSM *pFsm )
{
	pFsm->last_substate = &m_substate_Cooling;
}

/*
 * β[σΤ
 */

/* ^]ΨΦCxg */
PRIVATE void SubStateCooling_switch( FSM *pFsm )
{
	printf("  SubState: Cooling -> Warming\n");
	FSM_change_substate( pFsm, &m_substate_Warming );
}

/*
 * g[σΤ
 */

/* ^]ΨΦCxg */
PRIVATE void SubStateWarming_switch( FSM *pFsm )
{
	printf("  SubState: Warming -> Dehumidifying\n");
	FSM_change_substate( pFsm, &m_substate_Dehumidifying );
}

/*
 * ΌσΤ
 */

/* ^]ΨΦCxg */
PRIVATE void SubStateDehumidifying_switch( FSM *pFsm )
{
	printf("  SubState: Dehumidifying -> Cooling\n");
	FSM_change_substate( pFsm, &m_substate_Cooling );
}


