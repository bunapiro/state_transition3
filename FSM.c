#include <stdio.h>
#include "../../include/local.h"
#include "FSM.h"
#include "State.h"

/* FSM = Finite State Machine = 有限状態機械 */

PUBLIC void FSM_init( FSM *pSelf )
{
	State_init( pSelf );
}

/* 運転開始イベント */
PUBLIC void FSM_run( FSM *pSelf )
{
	if (   ( pSelf->current_state != NULL )
        && ( pSelf->current_state->event_run != NULL ) )
    {
		printf("Event: Run\n");
		pSelf->current_state->event_run( pSelf );
	}
}

/* 運転停止イベント */
PUBLIC void FSM_stop( FSM *pSelf )
{
	if (   (pSelf->current_state)
        && (pSelf->current_state->event_stop) )
    {
		printf("Event: Stop\n");
		pSelf->current_state->event_stop( pSelf );
	}
}

/* 運転切替イベント */
PUBLIC void FSM_switch( FSM *pSelf )
{
	if (   (pSelf->current_substate)
        && (pSelf->current_substate->event_switch) )
    {
		printf("Event: Switch\n");
		pSelf->current_substate->event_switch( pSelf );
	}
}

/* 状態遷移(共通) */
PRIVATE void change_state_common( FSM *pSelf, const State **ppCurrent, const State *pNew_state )
{
    /* exitがNULLでなければexitを実行 */
	if (   ( *ppCurrent != NULL )
        && ( (*ppCurrent)->exit != NULL ) )
    {
		(*ppCurrent)->exit( pSelf );
	}

    /* 現在の状態を新しい状態に更新 */
	*ppCurrent = pNew_state;

    /* 新しい状態へのentryがNULLでなければentryを実行 */
	if (   ( pNew_state != NULL )
        && ( pNew_state->entry != NULL ) )
    {
		pNew_state->entry( pSelf );
	}
}

/* 状態遷移 */
PUBLIC void FSM_change_state( FSM *pSelf, const State *pNew_state )
{
	change_state_common( pSelf, &(pSelf->current_state), pNew_state );
}

/* 状態遷移(サブ状態用) */
PUBLIC void FSM_change_substate( FSM *pSelf, const State *pNew_state )
{
	change_state_common( pSelf, &(pSelf->current_substate), pNew_state );
}


