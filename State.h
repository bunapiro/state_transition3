#ifndef STATE_H_INCLUDED
#define STATE_H_INCLUDED

#include "FSM.h"

/* 状態クラス */
typedef struct State {

	void (*entry)(FSM *fsm);        /* 入状イベント */
	void (*exit)(FSM *fsm);         /* 出状イベント */
	void (*event_run)(FSM *fsm);    /* 運転開始イベント */
	void (*event_stop)(FSM *fsm);   /* 運転停止イベント */
	void (*event_switch)(FSM *fsm); /* 運転切替イベント */

} State;

PUBLIC void State_init( FSM *pFsm );

#endif /* STATE_H_INCLUDED */


