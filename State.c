#include <stdio.h>
#include "../../include/local.h"
#include "State.h"

PRIVATE void StateStopped_run( FSM *pFsm );

PRIVATE void StateRunning_entry( FSM *pFsm );
PRIVATE void StateRunning_exit( FSM *pFsm );
PRIVATE void StateRunning_stop( FSM *pFsm );

/* 各Stateの唯一のオブジェクト */       /* entry */         /* exit */          /* event_run */     /* event_stop */    /* event_switch */
PRIVATE const State m_state_Stopped = { NULL                ,NULL               ,StateStopped_run   ,NULL               ,NULL };
PRIVATE const State m_state_Running = { StateRunning_entry  ,StateRunning_exit  ,NULL               ,StateRunning_stop  ,NULL };

/* 初期状態の設定 */
PUBLIC void State_init( FSM *pFsm )
{
	void init_last_substate( FSM *pFsm );           /* 多分関数ポインタの宣言。SubState.cのinit_last_substate()が公開されていればこの記述はいらないはず */

	pFsm->current_state     = &m_state_Stopped;
	pFsm->current_substate  = NULL;                 /* NULLだと何のイベントも処理しない */

	init_last_substate( pFsm );
}

/*
 * 停止中状態
 */

/* 運転開始イベント */
PRIVATE void StateStopped_run( FSM *pFsm )
{
	printf("  State: Stopped -> Running\n");
	FSM_change_state( pFsm, &m_state_Running );
}


/*
 * 運転中状態(コンポジット状態)
 */

/* 入状イベント */
PRIVATE void StateRunning_entry( FSM *pFsm )
{
	/* サブ状態をNULLから前回の状態に遷移(サブ状態のentryがあれば呼ばれる) */
    /* 「停止」状態から「運転」状態になったので、サブ状態は前回のサブ状態に復帰する */
	FSM_change_substate( pFsm, pFsm->last_substate );
}

/* 出状イベント */
PRIVATE void StateRunning_exit( FSM *pFsm )
{
	/* 現在のサブ状態を保存 */
	pFsm->last_substate = pFsm->current_substate;
	/* 運転中状態以外の時にサブ状態へイベントが飛ばないようにするために、
	 * サブ状態をNULLにする(サブ状態のexitがあれば呼ばれる) */
	FSM_change_substate( pFsm, NULL );
}

/* 運転停止イベント */
PRIVATE void StateRunning_stop( FSM *pFsm )
{
	printf("  State: Running -> Stopped\n");
	FSM_change_state( pFsm, &m_state_Stopped );
}


